package function

import (
	"chi-sample/er"
	"chi-sample/model"
	"chi-sample/util"
	"chi-sample/validator"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

// GetUserReq is request for get user
type GetUserReq struct {
	ID int `validate:"gte=0,lte=9999"`
}

// GetUser is function for get user
func GetUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "user_id"))
	if err != nil {
		util.RespondErrorOf(w, er.BadRequest)
		return
	}

	req := GetUserReq{id}

	validator.Validate(req)
	if err != nil {
		util.RespondErrorOf(w, er.BadRequest)
		return
	}

	user, err := getUser(&req)
	if err != nil {
		util.RespondError(w, err)
		return
	}
	util.RespondOK(w, user)
}

// return dummy
func getUser(req *GetUserReq) (*model.User, error) {
	return &model.User{
		ID:       req.ID,
		Name:     "John",
		Email:    "John@sample.com",
		Birthday: "2000-10-15",
	}, nil
}
