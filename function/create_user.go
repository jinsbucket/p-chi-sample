package function

import (
	"chi-sample/er"
	"chi-sample/util"
	"net/http"
)

// CreateUserReq is request for create user
type CreateUserReq struct {
	Name     string `json:"name" validate:"required,alpha"`
	Email    string `json:"email" validate:"required,email"`
	Birthday string `json:"birthday" validate:"datetime"`
}

// CreateUser is function for create user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	var req CreateUserReq
	err := util.BindBody(r, &req)
	if err != nil {
		util.RespondErrorOf(w, er.BadRequest)
		return
	}

	// Do create

	util.Respond(w, 200, "ok")
}
