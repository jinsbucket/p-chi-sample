package validator

import (
	"time"

	"gopkg.in/go-playground/validator.v9"
)

const (
	// ValidateDatetimeTag is a tagname for validation of datetime
	ValidateDatetimeTag = "datetime"
	layout              = "2006-01-02T15:04:05Z"
)

func validateDateTime(fl validator.FieldLevel) bool {

	value := fl.Field().String()
	_, err := time.Parse(layout, value)
	if err != nil {
		return false
	}

	return true
}
