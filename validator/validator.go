package validator

import (
	"log"

	"gopkg.in/go-playground/validator.v9"
)

var v *validator.Validate

// Validate is a function for validate struct
func Validate(s interface{}) error {
	if v == nil {
		initialize()
	}
	return v.Struct(s)
}

func initialize() {
	log.Print("init")
	v = validator.New()
	v.RegisterValidation(ValidateDatetimeTag, validateDateTime)
}
