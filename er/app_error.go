package er

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	// BadRequest is const for 400 error
	BadRequest = "0001|400|BadRequest"

	// UnExpectedError is const for 500 error
	UnExpectedError = "9999|500|Unexpectd error occurred"
)

// AppError is custom error
type AppError struct {
	StatusCode int
	*Body
}

// Body reprecents response body
type Body struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (e *AppError) Error() string {
	return fmt.Sprintf("Error. Code: %v, Message: %v", e.Body.Code, e.Body.Message)
}

// NewError creates new AppError from error kind string
func NewError(kind string) *AppError {
	split := strings.Split(kind, "|")
	statusCode, err := strconv.Atoi(split[1])
	if err != nil {
		panic(fmt.Sprintf("invalid error definition: %v", kind))
	}

	return &AppError{
		StatusCode: statusCode,
		Body: &Body{
			Code:    split[0],
			Message: split[2],
		},
	}
}
