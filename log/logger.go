package log

import (
	"github.com/hashicorp/logutils"
	"log"
	"os"
	"runtime"
)

const (
	LTrace = "trace"
	LDebug = "debug"
	LWarn  = "warn"
	LError = "error"
	LFatal = "fatal"
)

func Init(minLevel string) {
	filter := &logutils.LevelFilter{
		Levels: []logutils.LogLevel{
			LTrace, LDebug, LWarn, LError, LFatal,
		},
		MinLevel: logutils.LogLevel(minLevel),
		Writer:   os.Stdout,
	}
	log.SetOutput(filter)
}

func write(level string, msg string) {
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	}
	short := file
	for i := len(file) - 1; i > 0; i-- {
		if file[i] == '/' {
			short = file[i+1:]
			break
		}
	}
	file = short
	log.Printf("[%s] %s:%d - %s", level, short, line, msg)
}

func Trace(msg string) {
	write(LTrace, msg)
}

func Debug(msg string) {
	write(LDebug, msg)
}

func Warn(msg string) {
	write(LWarn, msg)
}

func Error(msg string) {
	write(LError, msg)
}

func Fatal(msg string) {
	write(LFatal, msg)
}
