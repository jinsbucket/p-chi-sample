package main

import (
	"chi-sample/function"
	"github.com/go-chi/chi"
	"net/http"
)

func main() {
	r := chi.NewRouter()
	r.Route("/v1/users", func(r chi.Router) {
		r.Get("/{user_id}", function.GetUser)
		r.Post("/", function.CreateUser)
	})
	http.ListenAndServe(":3000", r)
}
