package util

import (
	"chi-sample/er"
	"chi-sample/validator"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Respond is function for write response
func Respond(w http.ResponseWriter, statusCode int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	b, err := json.Marshal(body)
	if err != nil {
		panic("cannot encode")
	}
	w.Write(b)
}

// RespondOK is function for write response of 200
func RespondOK(w http.ResponseWriter, body interface{}) {
	Respond(w, 200, body)
}

// RespondError is function for write response of 500
func RespondError(w http.ResponseWriter, err error) {
	var appError *er.AppError
	var ok bool
	if appError, ok = err.(*er.AppError); !ok {
		appError = er.NewError(er.UnExpectedError)
	}
	Respond(w, appError.StatusCode, appError.Body)
}

// RespondErrorOf is function for write response
// errorKind is the kind of AppError
func RespondErrorOf(w http.ResponseWriter, errorKind string) {
	RespondError(w, er.NewError(errorKind))
}

// BindBody is function for bind request body with struct
func BindBody(r *http.Request, i interface{}) error {
	// bind
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	json.Unmarshal(b, i)

	// validate
	err = validator.Validate(i)
	if err != nil {
		return err
	}
	return nil
}
