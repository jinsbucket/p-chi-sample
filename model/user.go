package model

// User reprecents user
type User struct {

	// ID is user ID
	ID int `json:"id"`

	// Name is user name
	Name string `json:"name"`

	// Email is email address of the user
	Email string `json:"email"`

	// Birthday is birthday of the user
	Birthday string `json:"birthday"`
}
